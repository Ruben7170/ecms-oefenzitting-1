clear;

g=9.81;
rho=1000;
p_wzl=30*10^3;
p_zr=10^5;

%T=20�C
p_damp20=2.339 *10^3; % druk 2-fasegebied water in thermotech. tab 
h20=(p_zr-p_wzl-p_damp20)/(rho*g)-(10*10^3)/(rho*g)

%T=50�C
p_damp50=12.351*10^3;
h50=(p_zr-p_wzl-p_damp50)/(rho*g)-(10*10^3)/(rho*g)

%T=90�C
p_damp90=70.18*10^3;
h90=(p_zr-p_wzl-p_damp90)/(rho*g)-(10*10^3)/(rho*g)