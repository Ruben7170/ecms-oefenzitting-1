clear;

g=9.81;
rho=1000;
h=9;
eff=0.65;

Q=(500*10^(-3))/(10*60);   % in [m^3/s]
Pman=rho*g*h+30*10^3+40*10^3; % Manometrische druk die pomp moet leveren
                              % gelijk aan het hoogteverschil (in Pa)
                              % samen met verliezen in leidingen
P=Pman*Q/eff