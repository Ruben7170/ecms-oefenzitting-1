clear;

g=9.81;
rho=1000;
h=9;
eff=0.65;

Q=(500*10^(-3))/(10*60);   % in [m^3/s]
Pman=10^5+rho*g*h+30*10^3+40*10^3; % er is overdruk van 10^5 Pa
P=Pman*Q/eff