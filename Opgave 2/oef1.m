clear;

rho=1000;
g=9.81;

Q=35*10^(-3);
h_z=3-1;
D_in=0.15;
omega_in=1/4*pi*(D_in)^2; % doorsnede leiding
v_in=Q/omega_in;

p_z_res=10^5; % druk in reservoir aan zuigkant
p_w_zl=0.91*rho*(v_in^2)/2; % drukverlies in zuigluiding door wrijving
p_w_bocht=0.2*rho*(v_in^2)/2; % drukverlies door bocht in leiding
p_w_klep=0.9*rho*(v_in^2)/2; % drukverlies door klep
p_w_filter=2*rho*(v_in^2)/2; % drukverlies door filter

p_z_in=p_z_res-(rho*g*h_z+p_w_zl+2*p_w_bocht+p_w_klep+p_w_filter)
% absolute zuigdruk aan ingang
