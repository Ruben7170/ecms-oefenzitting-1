clear;

rho=1000;
g=9.81;

Q=35*10^(-3);
h_z=3-1;
h_p=12-3;
D_in=0.15;
omega_in=1/4*pi*(D_in)^2; % doorsnede leiding
v_in=Q/omega_in;

p_z_res=10^5; % druk in reservoir aan zuigkant
p_p_res=10^5; % druk in reservoir aan perskant
p_w_pl=4.08*rho*(v_in^2)/2; % drukverlies in persluiding door wrijving
p_w_bocht=0.2*rho*(v_in^2)/2; % drukverlies door bocht in leiding
p_w_klep=0.9*rho*(v_in^2)/2; % drukverlies door klep
p_w_filter=2*rho*(v_in^2)/2; % drukverlies door filter
p_w_zl=0.91*rho*(v_in^2)/2; % drukverlies in zuigleiding door wrijving

p_p_uit=p_p_res+rho*g*h_p+3*p_w_bocht+p_w_klep+p_w_pl; % absolute persdruk
p_z_in=p_z_res-(rho*g*h_z+p_w_zl+2*p_w_bocht+p_w_klep+p_w_filter);
% absolute zuigdruk

p_man=p_p_uit-p_z_in % manometrische opvoerdruk is verschil tussen pers-
                     % en zuigdruk