clear;

eps=0.04;
eff=0.95;
m_comp=1.25;
m_exp=1.15;

% Drukverhouding = 2
p_3_4=2;
V_4_3=(p_3_4)^(1/m_exp);

V_2_1=eps/(1+eps);
V_4_1=V_4_3*V_2_1;

eff_2=(1-V_4_1)/(1-V_2_1)*eff

% Drukverhouding = 5
p_3_4=5;
V_4_3=(p_3_4)^(1/m_exp);

V_2_1=eps/(1+eps);
V_4_1=V_4_3*V_2_1;

eff_5=(1-V_4_1)/(1-V_2_1)*eff