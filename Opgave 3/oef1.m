clear; % notatie zoals in oefenzitting

rho=1000;
n=960; % toerental in omw/min
D_uit=350*10^(-3); % buitendiameter in m
beta_2 =150*2*pi/360; % schoephoek aan uittredezijde in rad
b=20*10^(-3); % breedte aan uittredezijde in m
lek_rend=0.95; % lekrendement
k=0.8; % schoepfactor
hydr_rend=0.94; % hydraulisch rendement
p_man=160*10^3; % manometrische opvoerdruk

u_2 = pi*D_uit*n/60;
c_2u=p_man/(hydr_rend*k*u_2*rho);

w_2=(c_2u-u_2)/cos(beta_2);

c_2n=w_2-sin(beta_2); % fout?

Q=(pi*p_man*b*c_2n)/(lek_rend); % fout?



% oplossing Dropbox
w_2u=u_2-c_2u;
c_2n=w_2u*tan(beta_2);

Q=abs(lek_rend*c_2n*pi*D_uit*b) % we zijn ge�nteresseerd in de absolute waarde
                                % van het debiet