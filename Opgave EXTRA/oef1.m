clear;

syms H Q
eq1 = H == 8 - (Q^2) / 8;
eq2 = H == 2 + (Q^2) / 4;

[solvQ,solvH] = solve([eq1,eq2],[Q,H])